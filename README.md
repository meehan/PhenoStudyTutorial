# Introduction

# What will I get when I first set this up?

# First time setup

## Make Workspace
Make a new directory where you will clone this project to and other similar setups that will need to use Pythia and fastjet
```
mkdir PhenoStudy
cd PhenoStudy
```

## Get This Package
Clone this package into your newly created workspace ```PhenoStudy```.
```
git clone https://gitlab.cern.ch/meehan/PhenoStudyTutorial.git MyPhenoStudy
cd MyPhenoStudy
```

## Get External Packages
After checking out the code which allows you to see this README, you must install three third party sources of code that are used by this outlined below with tips from our experience.  Note that these must be housed at the same level as the TelescopingJets code that you checked out.  This is particularly true in the case of event generation with Pythia8 

### Pythia8: http://home.thep.lu.se/~torbjorn/Pythia.html 
- No tips here. Follow the directions on the included README file.
- If you are not familiar with Pythia then it is worthwhile working through the worksheet outlined here http://home.thep.lu.se/~torbjorn/pythia8/worksheet8183.pdf since the EventGeneration code is built starting from this.

### fastjet : http://fastjet.fr/
- Check out the latest stable version - I have it working with fastjet-3.2.1.
- Follow the directions on the included INSTALL file.

NOTE : I am installing this on Mac OSX and when running the ```make install``` command, it failed giving this error.

```
 ../../.././install-sh -c -d '/usr/local/include/fastjet/internal'
 /usr/bin/install -c -m 644 base.hh ClosestPair2DBase.hh Dnn2piCylinder.hh Dnn4piCylinder.hh DynamicNearestNeighbours.hh numconsts.hh Triangulation.hh BasicRandom.hh ClosestPair2D.hh Dnn3piCylinder.hh DnnPlane.hh LimitedWarning.hh MinHeap.hh SearchTree.hh Voronoi.hh IsBase.hh LazyTiling9.hh LazyTiling9Alt.hh LazyTiling25.hh LazyTiling9SeparateGhosts.hh TilingExtent.hh deprecated.hh '/usr/local/include/fastjet/internal'
install: /usr/local/include/fastjet/internal/base.hh: Permission denied
make[4]: *** [install-fastjetincludeinternalHEADERS] Error 71
make[3]: *** [install-am] Error 2
make[2]: *** [install-recursive] Error 1
make[1]: *** [install-recursive] Error 1
make: *** [install-recursive] Error 1
```
to circumvent this, I had to run the command as ```*sudo* make install```, which worked.

### fastjet contrib : http://fastjet.hepforge.org/contrib/ \\
- Follow the included README.
- When installing, it is necessary to point the code to the fastjet install directory as shown in the install directions.  This is done via the [--fastjet-config=FILE] argument.  For me, on Mac OSX, when I installed fastjet, this meant pointing it to the fastjet-config at ../fastjet-3.1.1/fastjet-config.  

```
./configure --fastjet-config=/Users/meehan/work/PhenoStudy/fastjet-3.2.1/fastjet-config
```

NOTE : I am installing this on Mac OSX and when running the ```make install``` command, it failed giving this error.

```
for header in ClusteringVetoPlugin.hh; do\
	  /bin/sh ../utils/install-sh -c -m 644 $header /usr/local/include/fastjet/contrib/;\
	done
cp: /usr/local/include/fastjet/contrib//_inst.88683_: Permission denied
make[1]: *** [install] Error 1
make: *** [ClusteringVetoPlugin] Error 2
```
to circumvent this, I had to run the command as ```*sudo* make install```, which worked.

If you do this correctly, then you should see the ```/contrib``` directory housed in ```/usr/local/include/fastjet/.```


# Running EventGeneration (Ana_EventGeneration)

This is the first state of analysis in which the Pythia8 event generator is used to 
produce a set of events for any process you wish and perform the subsequent shower 
and hadronization to produce a set of final state particles that can then be used 
for analysis via jet building.

This will generate 100 events and save to the pythia_wz.root output file a flat ntuple 
that contains truth vectors of the relevant particles (W,Z) for truth tagging later 
along with vector<double> branches for all of the truth particles with final state particles.



#### How : 
Go into the **Ana_EventGeneration** directory, compile the code and then run it

```
cd Ana_EventGeneration
source compile.sh
./MakeNTupleFromPythia 1 pythia_wz.root 100
```

**NOTE** : You may need to change the path to Pythia that is used or the compilation 
will fail.  Currently it is pointing to ```pythia8219``` but you may have 
downloaded a newer version.


# Running NTupling (Ana_NTupleMaker)

This is the second stage of analysis that takes as input the set of final state 
particles from Pythia and runs jet building and calculation of jet moments (substructure) 
on these jets using fastjet and fastjet contrib.  In particular, the Telescoping 
Jets code is run at this level of analysis.  This should always be made to produce 
a flat ntuple that has no complicated structure to it but is composed simply of 
branches of doubles.

This will process the 100 events generated with the ```MakeNTupleFromPythia``` code in the 
previous step and do the following:
1) run jet finding on the truth constituents 
2) determine truth flavor of jets via dR matching using the truth 4-vectors from the (W,Z)
3) calculate basic substructure quantities for these jets
4) Save the output in the file ```NTuple_wz.root```

### How : 
Go into the Ana_NTupleMaker directory, compile the code, and run it. Note that you are pointing the code to the previously generated Pythia event file.

```
cd ../Ana_NTupleMaker
source compile.sh
./NTupler 1 ../Ana_EventGeneration/pythia_wz.root NTuple_wz.root
```


# Running MiniNTupleAnalysis (Ana_MiniNTupleAnalysis)

This is the final stage of analysis where the analyzer uses any method they wish to 
analyze the flat ntuple of jets from the previous stage and produce final results.  
The preferred method is to use the TTree::Draw method (https://root.cern.ch/root/html/TTree.html#TTree:Draw@2) 
to quickly make histograms that can be used for further calculation.  This means that 
in principle there should be **no event loop** in this part of the code.  This is made 
more amenable by implementing all analysis tools with PyROOT (https://root.cern.ch/pyroot).

### How : 
Go into the **Ana_MiniNTupleAnalysis** directory, copy the previously produced file, 
and run the example script.

```
cd Ana_MiniNTupleAnalysis
cp cp ../Ana_NTupleMaker/NTuple_wz.root .
python DrawExampleHistogram.py
```

This will produce a single plot of the jet mass from the signal file that you produced.  The main advantage of these scripts
is that they have standard styles implemented and can be easily scripted together.  It is these scripts that are used to produce 
final result plots.

There are further scripts that do standard performance evaluation things like
- MakeROCS.py : Make ROC curves given signal and background and an observable

# Useful Links if You Are New To GitLab
- Matt's GitGuide : https://mattleblanc.github.io/git-guide/
- Markdown Help (for README) : https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet
