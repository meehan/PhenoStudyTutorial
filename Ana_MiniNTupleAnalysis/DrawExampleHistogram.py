import os
import sys
import itertools
from ROOT import *
from MyLocalFunctions import *
from AtlasStyle import *
SetAtlasStyle();
gStyle.SetPalette(1)

#manually set input parameters
InputDir="./"
alg = "TruthRaw"
pt1 = "100"
pt2 = "1000"
m1="0"
m2="1000"
variable = "m"
range="100,0,200"

#dictate output directory and make a new directory by that name
outputdir = "Plots/"
MakeNewDir(outputdir)
    
c = TCanvas("c","c",300,300)

#a weight that is used for event weights or for implementing simple cuts
weight=""
weight+="("
weight+=alg+"_pt>"+pt1+" && "
weight+=alg+"_pt<"+pt2
weight+=")"

#Get signal and background histograms
histname = alg+"_"+variable
print histname
hsig = GetHist1D(InputDir+"NTuple_wz.root",    "JetTree", histname, range, weight)

#Normalize them to unity
hsig = NormalizeHist(hsig)

# Style
print variable
hsig.GetXaxis().SetTitle(variable)
hsig.GetXaxis().SetTitleOffset(1.2)
hsig.GetYaxis().SetTitleOffset(1.7)
hsig.GetYaxis().SetTitle("Normalised Entries")
hsig.SetLineColor(2)
hsig.SetLineWidth(4)
hsig.SetLineStyle(2)
hsig.GetXaxis().SetTitle(variable)
hsig.GetYaxis().SetTitleOffset(1.6)
hsig.SetFillColor(2)
hsig.SetLineColor(2)
hsig.SetLineStyle(1)
hsig.SetFillStyle(3001)
hsig.SetMarkerSize(0)

maxval = GetMaxVal([hsig, hsig])
hsig.SetMaximum(maxval*2.0)
hsig.SetMinimum(0.001)

hsig.Draw("hist")

ATLASLabel(   0.20,0.85,1,0.1,0.03,"#sqrt{s}=13 TeV")
myText(       0.20,0.80,1,0.03, TranslateAlg(alg))
myText(       0.20,0.75,1,0.03, TranslateRegion(pt1,pt2,m1,m2))
c.SaveAs(outputdir+"example_"+alg+"_"+variable+"_pt"+pt1+pt2+".eps")

